import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import os


def prepare_data():
    transform = transforms.Compose(
        [transforms.ToTensor(),
         transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    trainset = torchvision.datasets.CIFAR10(root='./data', train=True,
                                            download=True, transform=transform)



    trainloader = torch.utils.data.DataLoader(trainset, batch_size=2,
                                              shuffle=True, num_workers=1)

    testset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                           download=True, transform=transform)
    testloader = torch.utils.data.DataLoader(testset, batch_size=2,
                                             shuffle=False, num_workers=1)

    classes = ('plane', 'car', 'bird', 'cat',
               'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

    return classes, trainloader, testloader


def imshow(img):
    img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()


def visual_some_image():
    #get some random training images
    dataiter = iter(trainloader)
    images, labels = dataiter.next()
    #show images
    imshow(torchvision.utils.make_grid(images))
    # print labels
    print(' '.join('%5s' % classes[labels[j]] for j in range(4)))


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x



classes, trainloader, testloader = prepare_data()

net = Net()

# loss function and optimizer
criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

losses = []

list_check_point = sorted(os.listdir('checkpoint-folder'))
count_checkpoint_file = len(os.listdir('checkpoint-folder'))

PATH_PRETRAIN = 'pretrain-folder/'
PATH_CHECKPOINT  = 'checkpoint-folder/'

load_checkpoint_path = PATH_CHECKPOINT + list_check_point[-1]

try:
    checkpoint = torch.load(load_checkpoint_path)
    net.load_state_dict(checkpoint['model_state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    loss = checkpoint['loss']
    # print('load checkpoint path: '+ load_checkpoint_path)
    # print(checkpoint)
except Exception:
    print(Exception)



def train(epochs, trainloader, testloader):
    for epoch in range(epochs):  # loop over the dataset multiple times
        net.train()
        running_loss = 0.0
        # for one batch
        for i, data in enumerate(trainloader, 0):
            # get the inputs; data is a list of [inputs, labels]
            inputs, labels = data

            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            # print('outputs: ', outputs)
            # print('labels: ', labels)
            loss.backward()
            optimizer.step()

            # print statistics
            running_loss += loss.item()
            losses.append(running_loss)
            if i % 2000 == 1999:    # print every 2000 mini-batches
                print('[epoch %d, batch %5d] loss: %.3f' %
                      (epoch + 1, i + 1, running_loss / 2000))
                running_loss = 0.0
                # validation loss
                net.eval()
                with torch.no_grad():
                    validation_loss = sum(criterion(net(xb), yb) for xb, yb in testloader)
                    validation_loss /= len(testloader)
                    print('validation_loss: ', validation_loss)

                if (running_loss / 2000) <= min(losses):
                    try:
                        # save pretrained model and checkpoint
                        pretrain_path = PATH_PRETRAIN + 'pretrain.pt'
                        checkpoint_path = PATH_CHECKPOINT + 'checkpoint-' + str(count_checkpoint_file) + '.pt'
                        # save entire model
                        torch.save(net, pretrain_path)
                        #save checkpoint
                        torch.save({
                            'model_state_dict': net.state_dict(),
                            'optimizer_state_dict': optimizer.state_dict(),
                            'loss': loss
                        }, checkpoint_path)
                        print('save model and checkpoint succesfully')
                    except:
                        print('save model false')

    print('Finished Training')

epochs = 5
train(epochs, trainloader, testloader)



